# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/yandex-cloud/yandex" {
  version = "0.91.0"
  hashes = [
    "h1:4s+FoYf6bd3ERi4p9l+gRmCXE6Q/s4mDJZ6H1ICTy5c=",
    "zh:0eb744352ba5b2e66c9284c0d4277c4061eb159961dcad9bb11e7be8bf3f6c5c",
    "zh:1a9d9a8bd3f402c96dc951dbb311ed019740cc8ba5ec1bf530bac2e4adb789cf",
    "zh:1d9868ee308d65a0c1f8f0e5e5ae709774348e99b3ce704a32edea153bb7fe68",
    "zh:35d6499910556e2240baf473e6198bd061762b23905a343d1f348c1da7f2a009",
    "zh:4a0f10f692f0e53c3f2d7bb4eb3bb0ea69174526f7e5b6ac001effcaa8fd2b31",
    "zh:4ea8915e0ab3b4e9c2e6079cba259ae67efba2dfae1e7beacb87a894cbbfd465",
    "zh:78e29ec49b2eb606c57a14bf334b4c48268b2d4cbf02e40d19924f680b17816f",
    "zh:b5cc7bf8c53f0d985caf70b4fe32608576ee0f713ce6973b5f4da249d7b2bbd4",
    "zh:b9084059afa263cdf6e8477742b8656517a0e43d2983a1a80c2fb671e22301b8",
    "zh:cfe255359d221886a883df12722b7ef4331fe7b7bb314386842e5dddc18ec42e",
    "zh:d8d19c293f727b2ddae40e5e637b60a143e2ba11f1e048b97aa4be32e5c33d25",
    "zh:db13a9904074856ee3ce73674287016dfd95501542f50c821166c937109758f3",
    "zh:e6216c34d8c6ec5d317e16049a16f5eab5d9978678793d1050ec57af84b720ca",
  ]
}
