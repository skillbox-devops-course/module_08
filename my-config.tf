 terraform {
   required_providers {
     yandex = {
       source = "yandex-cloud/yandex"
     }
   }
 }
  
 provider "yandex" {
   token  =  "y0_AgAAAAAHJ2qEAATuwQAAAADcU-mehHK-107WRPm_NNTM8G0w7MIn81Q"
   cloud_id  = "b1g44dq7dqc43fth3uph"
   folder_id = "b1g6lo620242k4ovqik0"
   zone      = "ru-central1-a"
 }
 

data "yandex_compute_image" "my_image" {
  family = "ubuntu-2204-lts"
}

resource "yandex_compute_instance_group" "ig-1" {
  name               = "fixed-ig-with-balancer3"
  folder_id          = "b1g6lo620242k4ovqik0"
  service_account_id = "aje0dkhbqqhot0v4oera"
  instance_template {
    # name = "Nginx-{instance.index}"
    platform_id = "standard-v1"
    resources {
      memory = 2
      cores  = 2
    }

    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = "${data.yandex_compute_image.my_image.id}"
      }
    }

    network_interface {
      network_id = "${yandex_vpc_network.network-1.id}"
      subnet_ids = ["${yandex_vpc_subnet.subnet-1.id}"]
      nat       = true
    }

    metadata = {
      user-data = "${file("user_data")}"
      # ssh-keys = "<имя пользователя>:<содержимое SSH-ключа>"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    zones = ["ru-central1-a"]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  load_balancer {
    target_group_name        = "target-group"
    target_group_description = "load balancer target group"
  }
}

resource "yandex_lb_network_load_balancer" "lb-1" {
  name = "network-load-balancer-1"

  listener {
    name = "network-load-balancer-1-listener"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.ig-1.load_balancer.0.target_group_id

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/index.nginx-debian.html"
      }
    }
  }
}
resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.network-1.id}"
  v4_cidr_blocks = ["192.168.10.0/24"]
}



resource "yandex_compute_instance" "default" {
  name        = "reactjsserver"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my_image.id}"
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    nat = true
  }

  metadata = {   
    ssh-keys = "ubuntu:${file("~/.ssh/ya-skillbox.pub")}"
  }
}


# output "internal_ip_address_vm_1" {
#   value = yandex_compute_instance_group.yandex_compute_instance.host vm-1.network_interface.0.ip_address
# }
 
output "external_ip_address_vm_1" {
  value = yandex_compute_instance.default.network_interface.0.nat_ip_address
} 